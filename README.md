# Discord MessagePurge
#### The following is an all in one Discord Message Purger. It is not a bot that has to be added but runs locally from your machine and can purge All your messages.This works in both Channels and Direct Messages.

### Installation
1. Install `nodejs` and `npm` on your machine.
2. Clone this repo and navigate to the newly created directory.
3. Type in `npm install`
4. Now you can run the code with `node discordpurge.js`

### How to find Token

1. Press `Ctrl+Shift+I`and navigate to the network tab
2. Press F5 in the network tab to reload the tab.
3. Go into any chat or channel and begin typing. A new entry will show up at the bottom of the Network tab called `typing`.
4. Click on `typing` and in Request Headers next to authorization it will be a long string of characters. This is your Token.


### Notice
In Direct Messages(Dm) you cannot fetch Messsages past a unknown threshold as such if Dm in question is an active Dm you will not be able to insure all older messages get deleted. I.e it will delete to the set threshold.