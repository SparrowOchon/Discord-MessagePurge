/**
 * FILENAME :        discordpurge.js
 *
 * DESCRIPTION :
 *        Purge Discord  Direct messages(channels) or Channels(guilds) based on user input.
 *         Can purge All Direct Messages who are currently Indexed for you. Channels are
 *         always indexed as such this does not suffer from this problem.
 *
 * NOTES :
 *   In Direct Messages(Dm) you cannot fetch Messsages past a unknown threshold as such
 *     if Dm in question is an active Dm you will not be able to insure all older
 *     messages get deleted. I.e it will delete to the set threshold (3 Months).
 *
 *
 *       Copyright Network Silence. 2018.  All rights reserved.
 *
 * Show don't Sell License Version 1
 *   <https://gitlab.com/SparrowOchon/show-dont-sell/raw/master/LICENSE.md>
 *
 *
 * AUTHOR :   Network Silence        START DATE :    16 Nov 2018
 *
 */
const API_CALL = require("request-promise-native"); // Used to Post to Discord API
const CMD_LINE = require("inquirer"); // Used to get user-input
const PROGRESS_BAR = require("progress"); // Used for ProgressBar
const API_URL = "https://discordapp.com/api/v6/"; // Discord API Url

var headers = {}; //Post Headers
let ignoreChannels = {}; // Array of Channels based on index of Channels to Ignore

/**
 * A function that puts the program to sleep for ms amount of time
 *
 * @param  {integer} ms The amount of milliseconds to sleep.
 * @return {Promise Object} Promise object to check success of sleep.
 */
function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

/**
 * Fetch a list of the last 25 messages by your user on the Channel/Dm.
 *
 * @param  {string} type   Type of Chat I.e Dm or Channel
 * @param  {string} target Channel or DM id which you are trying to purge from.
 * @param  {string} user   The user whos messages we are trying to delete.(I.e you)
 * @return {Object array}  List of the last 25 messages posted by the current user (Includes System Messages)
 */
async function getMessages(type, target, user, limit) {
  return JSON.parse(
    await API_CALL({
      url:
        API_URL +
        type +
        "s/" +
        target +
        "/messages/search?author_id=" +
        user +
        "&include_nsfw=true&limit=" +
        limit,
      headers: headers
    })
  );
}

/**
 * Post request to Discord API to delete message.
 *
 * @param  {string} channelId  The id of the Chat you want to wipe.
 * @param  {string} messageId  The Id of the message you want to delete.
 * @return {}
 */
async function removeMessages(channelId, messageId) {
  await API_CALL({
    method: "DELETE",
    url: API_URL + "channels/" + channelId + "/messages/" + messageId,
    headers: headers
  });
}

/**
 * Use getMessages to get all the messages for the Target Channel/Dm draw a progress bar and
 *  send them to removeMessages to purge them.
 * NOTE:
 * Possible the Bar goes over for ~25 since there can be duplicates in the Last Fetch ex: when
 * the Threshold is reached for Dms.
 *
 * @param  {string} type   Chat type I.e Channel => Dms or Guild => Channels.
 * @param  {string} target Channel or Guild Id.
 * @param  {string} user   User token to identify self messages.
 * @return
 */
async function removeSetup(type, target, user) {
  let bar;
  let initialTotalMsgs = 0;
  let discordMessages;
  let isIndexed = false;
  let fetchLimit = 25; // Limited to 25 by discord
  do {
    discordMessages = await getMessages(type, target, user, fetchLimit);
    if (discordMessages.hasOwnProperty("document_indexed")) {
      console.log("Not indexed yet. Retrying in 2 seconds.");
      isIndexed = true;
      await sleep(100);
      continue;
    }
    isIndexed = false;
    let messages = discordMessages.messages;
    if (!bar) {
      bar = new PROGRESS_BAR(":bar :percent :current/:total eta: :eta s", {
        total: discordMessages.total_results
      });
      initialTotalMsgs = discordMessages.total_results; // Total messages you sent in channel/Direct Message.
    }
    // Merge messages into Singular
    messages = messages.map(messageArray => {
      return messageArray.reduce((accInfo, userMessage) => {
        if (userMessage.hit) {
          return userMessage;
        } else {
          return accInfo;
        }
      });
    });
    //Find out if Initial Total Msgs Changes in Group Chat when over 3 months.
    for (var i = 0; i < messages.length; i++) {
      bar.tick();
      if (messages[i].type !== 0 || ignoreChannels[messages[i].channel_id]) {
        //Type 0 is for messages all other types are for System Messages
        initialTotalMsgs--;
        continue;
      }
      try {
        await removeMessages(messages[i].channel_id, messages[i].id);
        initialTotalMsgs--;
      } catch (error) {
        //Server Error Normally Due to API limit
        //console.error(error);
        initialTotalMsgs++;
        i--; // Reset remove attempt
        console.log("Api spam limitter hit. Retrying in 5 seconds.");
        await sleep(6000);
      }
      await sleep(1000); //Limit of 1 delete call ever 500ms due to Discord Api.
    }
    if (initialTotalMsgs < fetchLimit) {
      fetchLimit = initialTotalMsgs;
    }
  } while (initialTotalMsgs !== 0 || isIndexed === true);
  console.log("Purge Complete!");
}

/**
 * Main driver function of this Program. Manages all User Input.
 * @return
 */
async function main() {
  var userInput = await CMD_LINE.prompt([
    {
      type: "input",
      name: "token",
      message: "Token:"
    }
  ]);
  headers = {
    Authorization: userInput.token
  };
  let user = JSON.parse(
    await API_CALL({
      url: API_URL + "/users/@me",
      headers: headers
    })
  );
  console.log("Logged in as: " + user.username + "#" + user.discriminator);
  userInput = await CMD_LINE.prompt([
    {
      type: "list",
      name: "type",
      message: "What would you like to delete?",
      choices: [
        {
          value: "guild",
          name: "Channel messages"
        },
        {
          value: "channel",
          name: "Direct Messages"
        }
      ]
    }
  ]);
  if (userInput.type == "channel") {
    let ignoreString = await CMD_LINE.prompt([
      {
        type: "input",
        name: "list",
        message: "Ignored Channels (seperate with commas; leave blank if none):"
      }
    ]);
    if (ignoreString.list && ignoreString.list !== "") {
      let listOfIgnoredItems = ignoreString.list.split(",");
      for (const item in listOfIgnoredItems) {
        ignoreChannels[listOfIgnoredItems[item].trim()] = true;
      }
    }
  }
  let type = userInput.type;
  let targets = JSON.parse(
    await API_CALL({
      url: API_URL + "/users/@me/" + type + "s",
      headers: headers
    })
  );
  //Array of all Servers/Dms User has open.
  targets = targets.map(x => {
    x.value = x.id;
    if (type == "channel") {
      x.name = x.recipients.reduce(
        (acc, y, i) =>
          (i != 0 ? acc + ", " : "") + y.username + "#" + y.discriminator,
        ""
      );
    }
    return x;
  });
  targets.unshift("All"); // Prepend "All" to Possible Targets List.
  userInput = await CMD_LINE.prompt([
    {
      type: "list",
      name: "target",
      message: type,
      choices: targets
    }
  ]);
  let chatToWipe = userInput.target; // No need to save in variable.
  userInput = await CMD_LINE.prompt([
    {
      type: "confirm",
      name: "confirm",
      message: "Are you sure? This will delete all of your messages.",
      default: true
    }
  ]);
  if (userInput.confirm) {
    if (chatToWipe == "All") {
      targets.shift();
      for (var i = 0; i < targets.length; i++) {
        otherMessages = 0;
        console.log(targets[i].name);
        await removeSetup(type, targets[i].id, user.id);
        await sleep(2000);
      }
    } else {
      await removeSetup(type, chatToWipe, user.id);
    }
  }
}

main();
